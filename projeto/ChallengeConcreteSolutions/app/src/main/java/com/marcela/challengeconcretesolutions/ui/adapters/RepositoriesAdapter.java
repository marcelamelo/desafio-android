package com.marcela.challengeconcretesolutions.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.marcela.challengeconcretesolutions.R;
import com.marcela.challengeconcretesolutions.commons.CircleTransform;
import com.marcela.challengeconcretesolutions.commons.RecyclerViewListener;
import com.marcela.challengeconcretesolutions.models.Repository;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by marcela on 15/12/16.
 */

public class RepositoriesAdapter extends RecyclerView.Adapter<RepositoriesAdapter.RepositoriesViewHolder>{

    public class RepositoriesViewHolder extends RecyclerView.ViewHolder{

        private Context context;

        @Bind(R.id.repository_row)
        RelativeLayout repository_row;
        @Bind(R.id.txt_repository_name)
        TextView txt_repository_name;
        @Bind(R.id.txt_repository_description)
        TextView txt_repository_description;
        @Bind(R.id.txt_count_fork)
        TextView txt_count_fork;
        @Bind(R.id.txt_count_star)
        TextView txt_count_star;
        @Bind(R.id.img_owner_avatar)
        ImageView img_owner_avatar;
        @Bind(R.id.txt_owner_username)
        TextView txt_owner_username;
        @Bind(R.id.txt_owner_fullname)
        TextView txt_owner_fullname;


        public RepositoriesViewHolder(Context context, View itemView) {
            super(itemView);
            this.context = context;
            ButterKnife.bind(this, itemView);
        }
    }

    private Context context;
    private ArrayList<Repository> items;
    private RecyclerViewListener listener;

    public RepositoriesAdapter(Context context, ArrayList<Repository> items, RecyclerViewListener listener) {
        this.context = context;
        this.items = items;
        this.listener = listener;
    }

    @Override
    public RepositoriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.repository_item, parent, false);
        RepositoriesViewHolder viewHolder = new RepositoriesViewHolder(context, view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RepositoriesViewHolder holder, final int position) {
        Repository repository = items.get(position);
        Glide.with(context).load(repository.getOwner().getAvatar_url()).centerCrop().transform(new CircleTransform(context)).skipMemoryCache(false).diskCacheStrategy(DiskCacheStrategy.RESULT).into(holder.img_owner_avatar);
        holder.txt_repository_name.setText(repository.getName());
        holder.txt_repository_description.setText(repository.getDescription());
        holder.txt_count_fork.setText(String.valueOf(repository.getForks_count()));
        holder.txt_count_star.setText(String.valueOf(repository.getStargazers_count()));
        holder.txt_owner_username.setText(repository.getOwner().getLogin());
        holder.txt_owner_fullname.setText(repository.getOwner().getName());

        holder.repository_row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener != null){
                    listener.onClick(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

}
