// Generated code from Butter Knife. Do not modify!
package com.marcela.challengeconcretesolutions.ui.activities;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class PullRequestsListActivity$$ViewBinder<T extends com.marcela.challengeconcretesolutions.ui.activities.PullRequestsListActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131492978, "field 'toolbar'");
    target.toolbar = finder.castView(view, 2131492978, "field 'toolbar'");
    view = finder.findRequiredView(source, 2131492979, "field 'content_header'");
    target.content_header = finder.castView(view, 2131492979, "field 'content_header'");
    view = finder.findRequiredView(source, 2131492983, "field 'progress'");
    target.progress = finder.castView(view, 2131492983, "field 'progress'");
    view = finder.findRequiredView(source, 2131492980, "field 'txt_opened_count'");
    target.txt_opened_count = finder.castView(view, 2131492980, "field 'txt_opened_count'");
    view = finder.findRequiredView(source, 2131492981, "field 'txt_closed_count'");
    target.txt_closed_count = finder.castView(view, 2131492981, "field 'txt_closed_count'");
    view = finder.findRequiredView(source, 2131492982, "field 'rv_pull_request'");
    target.rv_pull_request = finder.castView(view, 2131492982, "field 'rv_pull_request'");
  }

  @Override public void unbind(T target) {
    target.toolbar = null;
    target.content_header = null;
    target.progress = null;
    target.txt_opened_count = null;
    target.txt_closed_count = null;
    target.rv_pull_request = null;
  }
}
