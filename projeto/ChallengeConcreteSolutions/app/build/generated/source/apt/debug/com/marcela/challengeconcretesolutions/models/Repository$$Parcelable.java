
package com.marcela.challengeconcretesolutions.models;

import android.os.Parcelable;
import android.os.Parcelable.Creator;
import org.parceler.Generated;
import org.parceler.IdentityCollection;
import org.parceler.InjectionUtil;
import org.parceler.ParcelWrapper;
import org.parceler.ParcelerRuntimeException;

@Generated(value = "org.parceler.ParcelAnnotationProcessor", date = "2016-12-19T08:07-0200")
@SuppressWarnings({
    "unchecked",
    "deprecation"
})
public class Repository$$Parcelable
    implements Parcelable, ParcelWrapper<com.marcela.challengeconcretesolutions.models.Repository>
{

    private com.marcela.challengeconcretesolutions.models.Repository repository$$0;
    @SuppressWarnings("UnusedDeclaration")
    public final static Creator<Repository$$Parcelable>CREATOR = new Creator<Repository$$Parcelable>() {


        @Override
        public Repository$$Parcelable createFromParcel(android.os.Parcel parcel$$2) {
            return new Repository$$Parcelable(read(parcel$$2, new IdentityCollection()));
        }

        @Override
        public Repository$$Parcelable[] newArray(int size) {
            return new Repository$$Parcelable[size] ;
        }

    }
    ;

    public Repository$$Parcelable(com.marcela.challengeconcretesolutions.models.Repository repository$$2) {
        repository$$0 = repository$$2;
    }

    @Override
    public void writeToParcel(android.os.Parcel parcel$$0, int flags) {
        write(repository$$0, parcel$$0, flags, new IdentityCollection());
    }

    public static void write(com.marcela.challengeconcretesolutions.models.Repository repository$$1, android.os.Parcel parcel$$1, int flags$$0, IdentityCollection identityMap$$0) {
        int identity$$0 = identityMap$$0 .getKey(repository$$1);
        if (identity$$0 != -1) {
            parcel$$1 .writeInt(identity$$0);
        } else {
            parcel$$1 .writeInt(identityMap$$0 .put(repository$$1));
            com.marcela.challengeconcretesolutions.models.User$$Parcelable.write(InjectionUtil.getField(com.marcela.challengeconcretesolutions.models.User.class, com.marcela.challengeconcretesolutions.models.Repository.class, repository$$1, "owner"), parcel$$1, flags$$0, identityMap$$0);
            parcel$$1 .writeString(InjectionUtil.getField(java.lang.String.class, com.marcela.challengeconcretesolutions.models.Repository.class, repository$$1, "full_name"));
            parcel$$1 .writeInt(InjectionUtil.getField(int.class, com.marcela.challengeconcretesolutions.models.Repository.class, repository$$1, "stargazers_count"));
            parcel$$1 .writeString(InjectionUtil.getField(java.lang.String.class, com.marcela.challengeconcretesolutions.models.Repository.class, repository$$1, "name"));
            parcel$$1 .writeString(InjectionUtil.getField(java.lang.String.class, com.marcela.challengeconcretesolutions.models.Repository.class, repository$$1, "description"));
            parcel$$1 .writeInt(InjectionUtil.getField(int.class, com.marcela.challengeconcretesolutions.models.Repository.class, repository$$1, "id"));
            parcel$$1 .writeInt(InjectionUtil.getField(int.class, com.marcela.challengeconcretesolutions.models.Repository.class, repository$$1, "forks_count"));
        }
    }

    @Override
    public int describeContents() {
        return  0;
    }

    @Override
    public com.marcela.challengeconcretesolutions.models.Repository getParcel() {
        return repository$$0;
    }

    public static com.marcela.challengeconcretesolutions.models.Repository read(android.os.Parcel parcel$$3, IdentityCollection identityMap$$1) {
        int identity$$1 = parcel$$3 .readInt();
        if (identityMap$$1 .containsKey(identity$$1)) {
            if (identityMap$$1 .isReserved(identity$$1)) {
                throw new ParcelerRuntimeException("An instance loop was detected whild building Parcelable and deseralization cannot continue.  This error is most likely due to using @ParcelConstructor or @ParcelFactory.");
            }
            return identityMap$$1 .get(identity$$1);
        } else {
            com.marcela.challengeconcretesolutions.models.Repository repository$$4;
            int reservation$$0 = identityMap$$1 .reserve();
            repository$$4 = new com.marcela.challengeconcretesolutions.models.Repository();
            identityMap$$1 .put(reservation$$0, repository$$4);
            com.marcela.challengeconcretesolutions.models.User user$$0 = com.marcela.challengeconcretesolutions.models.User$$Parcelable.read(parcel$$3, identityMap$$1);
            InjectionUtil.setField(com.marcela.challengeconcretesolutions.models.Repository.class, repository$$4, "owner", user$$0);
            InjectionUtil.setField(com.marcela.challengeconcretesolutions.models.Repository.class, repository$$4, "full_name", parcel$$3 .readString());
            InjectionUtil.setField(com.marcela.challengeconcretesolutions.models.Repository.class, repository$$4, "stargazers_count", parcel$$3 .readInt());
            InjectionUtil.setField(com.marcela.challengeconcretesolutions.models.Repository.class, repository$$4, "name", parcel$$3 .readString());
            InjectionUtil.setField(com.marcela.challengeconcretesolutions.models.Repository.class, repository$$4, "description", parcel$$3 .readString());
            InjectionUtil.setField(com.marcela.challengeconcretesolutions.models.Repository.class, repository$$4, "id", parcel$$3 .readInt());
            InjectionUtil.setField(com.marcela.challengeconcretesolutions.models.Repository.class, repository$$4, "forks_count", parcel$$3 .readInt());
            com.marcela.challengeconcretesolutions.models.Repository repository$$3 = repository$$4;
            identityMap$$1 .put(identity$$1, repository$$3);
            return repository$$3;
        }
    }

}
