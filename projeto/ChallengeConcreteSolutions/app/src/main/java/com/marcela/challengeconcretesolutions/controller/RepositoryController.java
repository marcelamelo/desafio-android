package com.marcela.challengeconcretesolutions.controller;

import com.marcela.challengeconcretesolutions.commons.ChallengeConcreteSolutionsApiClient;
import com.marcela.challengeconcretesolutions.commons.ResponseListener;
import com.marcela.challengeconcretesolutions.models.ReturnRepository;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by marcela on 15/12/16.
 */

public class RepositoryController {

    public RepositoryController() {
        super();
    }

    public void getRepositories(int page, final ResponseListener listener){
        Service service = ChallengeConcreteSolutionsApiClient.createService(Service.class);

        Call<ReturnRepository> call = service.getRepositories("stars", page);
        call.enqueue(new Callback<ReturnRepository>() {
            @Override
            public void onResponse(Call<ReturnRepository> call, Response<ReturnRepository> response) {
                listener.success(response.body());
            }

            @Override
            public void onFailure(Call<ReturnRepository> call, Throwable t) {
                listener.fail(t);
            }
        });
    }
}
