package com.marcela.challengeconcretesolutions.commons;

/**
 * Created by marcela on 15/12/16.
 */

public interface ResponseListener {
    void success (Object object);
    void fail(Throwable throwable);
}
