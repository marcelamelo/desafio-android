package com.marcela.challengeconcretesolutions.commons;

/**
 * Created by marcela on 15/12/16.
 */

public class Constants {
    public static final String BASE_URL = "https://api.github.com";
    public static final String FULL_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
}
