package com.marcela.challengeconcretesolutions.commons;

/**
 * Created by marcela on 15/12/16.
 */

public interface RecyclerViewListener {
    void onClick(int position);
}
