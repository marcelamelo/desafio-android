package com.marcela.challengeconcretesolutions.commons;

import android.app.Application;
import android.content.Context;

import com.marcela.challengeconcretesolutions.R;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by marcela on 15/12/16.
 */

public class ChallengeConcreteSolutionsApplication extends Application{
    private static ChallengeConcreteSolutionsApplication instance;
    private static Context context;

    public static ChallengeConcreteSolutionsApplication getInstance() {
        return instance;
    }

    public static Context getContext() {
        return context;
    }

    @Override
    public void onCreate(){
        super.onCreate();

        instance = this;
        ChallengeConcreteSolutionsApplication.context = getApplicationContext();

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Roboto-Light.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }
}
