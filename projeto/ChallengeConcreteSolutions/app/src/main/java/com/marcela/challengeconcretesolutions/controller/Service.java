package com.marcela.challengeconcretesolutions.controller;

import com.marcela.challengeconcretesolutions.models.PullRequest;
import com.marcela.challengeconcretesolutions.models.ReturnRepository;
import com.marcela.challengeconcretesolutions.models.User;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by marcela on 15/12/16.
 */

public interface Service {
    @GET("/search/repositories?q=language:Java")
    Call<ReturnRepository> getRepositories (@Query("sort") String order,
                                            @Query("page") int page );

    @GET("/users/{user_name}")
    Call<User> getUser (@Path("user_name") String user_name);

    @GET("/repos/{user_name}/{repository}/pulls")
    Call<ArrayList<PullRequest>> getPullRequest (@Path("user_name") String user_name,
                                                 @Path("repository") String repository);
}
