package com.marcela.challengeconcretesolutions.controller;

import com.marcela.challengeconcretesolutions.commons.ChallengeConcreteSolutionsApiClient;
import com.marcela.challengeconcretesolutions.commons.ResponseListener;
import com.marcela.challengeconcretesolutions.models.PullRequest;
import com.marcela.challengeconcretesolutions.models.Repository;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by marcela on 15/12/16.
 */

public class PullRequestController {

    public PullRequestController() {
        super();
    }

    public void getPullRequests(Repository repository, final ResponseListener listener){
        Service service = ChallengeConcreteSolutionsApiClient.createService(Service.class);

        Call<ArrayList<PullRequest>> call = service.getPullRequest(repository.getOwner().getLogin(), repository.getName());
        call.enqueue(new Callback<ArrayList<PullRequest>>() {
            @Override
            public void onResponse(Call<ArrayList<PullRequest>> call, Response<ArrayList<PullRequest>> response) {
                listener.success(response.body());
            }

            @Override
            public void onFailure(Call<ArrayList<PullRequest>> call, Throwable t) {
                listener.fail(t);
            }
        });
    }
}
