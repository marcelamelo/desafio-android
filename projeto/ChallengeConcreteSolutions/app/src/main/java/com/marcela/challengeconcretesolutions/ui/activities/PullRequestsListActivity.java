package com.marcela.challengeconcretesolutions.ui.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.marcela.challengeconcretesolutions.R;
import com.marcela.challengeconcretesolutions.commons.RecyclerViewListener;
import com.marcela.challengeconcretesolutions.commons.ResponseListener;
import com.marcela.challengeconcretesolutions.controller.PullRequestController;
import com.marcela.challengeconcretesolutions.models.PullRequest;
import com.marcela.challengeconcretesolutions.models.Repository;
import com.marcela.challengeconcretesolutions.ui.adapters.PullRequestAdapter;

import org.parceler.Parcels;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

public class PullRequestsListActivity extends BaseActivity {

    Repository repository;

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.content_header)
    LinearLayout content_header;
    @Bind(R.id.progress)
    RelativeLayout progress;
    @Bind(R.id.txt_opened_count)
    TextView txt_opened_count;
    @Bind(R.id.txt_closed_count)
    TextView txt_closed_count;
    @Bind(R.id.rv_pull_request)
    RecyclerView rv_pull_request;

    ArrayList<PullRequest> pullRequests = new ArrayList<>();
    PullRequestAdapter adapter;

    int countOpened = 0;
    int countClosed = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_requests_list);
        ButterKnife.bind(this);

        repository = Parcels.unwrap(getIntent().getExtras().getParcelable("repository"));

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(repository.getName());
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        rv_pull_request.setLayoutManager(new LinearLayoutManager(this));

        getData();
    }

    public void getData(){
        progress.setVisibility(View.VISIBLE);
        new PullRequestController().getPullRequests(repository, new ResponseListener() {
            @Override
            public void success(Object object) {
                pullRequests = (ArrayList<PullRequest>) object;

                for(PullRequest pullRequest : pullRequests){
                    if (pullRequest.getState().equals("open")){
                        countOpened++;
                    }
                    else{
                        countClosed++;
                    }
                }
                txt_opened_count.setText(""+countOpened + " opened");
                txt_closed_count.setText(""+countClosed + " closed");
                content_header.setVisibility(View.VISIBLE);

                progress.setVisibility(View.GONE);

                adapter = new PullRequestAdapter(PullRequestsListActivity.this, pullRequests, new RecyclerViewListener() {
                    @Override
                    public void onClick(int position) {
                        if (isOline(PullRequestsListActivity.this)){
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setData(Uri.parse(pullRequests.get(position).getHtml_url()));
                            startActivity(intent);
                        }
                        else{
                            showDialogMessage("Falha de conexão", "Verifique sua internet e tente novamente.");
                        }
                    }
                });
                rv_pull_request.setAdapter(adapter);
                rv_pull_request.setItemAnimator(new DefaultItemAnimator());
            }

            @Override
            public void fail(Throwable throwable) {
                progress.setVisibility(View.GONE);
                if (isOline(PullRequestsListActivity.this)) {
                    showDialogMessage("", throwable.getMessage());
                } else {
                    showDialogMessage("Falha de conexão", "Verifique sua internet e tente novamente.");
                    finish();
                }
            }
        });
    }
}
