
package com.marcela.challengeconcretesolutions.models;

import android.os.Parcelable;
import android.os.Parcelable.Creator;
import org.parceler.Generated;
import org.parceler.IdentityCollection;
import org.parceler.InjectionUtil;
import org.parceler.ParcelWrapper;
import org.parceler.ParcelerRuntimeException;

@Generated(value = "org.parceler.ParcelAnnotationProcessor", date = "2016-12-19T08:07-0200")
@SuppressWarnings({
    "unchecked",
    "deprecation"
})
public class ReturnRepository$$Parcelable
    implements Parcelable, ParcelWrapper<com.marcela.challengeconcretesolutions.models.ReturnRepository>
{

    private com.marcela.challengeconcretesolutions.models.ReturnRepository returnRepository$$0;
    @SuppressWarnings("UnusedDeclaration")
    public final static Creator<ReturnRepository$$Parcelable>CREATOR = new Creator<ReturnRepository$$Parcelable>() {


        @Override
        public ReturnRepository$$Parcelable createFromParcel(android.os.Parcel parcel$$2) {
            return new ReturnRepository$$Parcelable(read(parcel$$2, new IdentityCollection()));
        }

        @Override
        public ReturnRepository$$Parcelable[] newArray(int size) {
            return new ReturnRepository$$Parcelable[size] ;
        }

    }
    ;

    public ReturnRepository$$Parcelable(com.marcela.challengeconcretesolutions.models.ReturnRepository returnRepository$$2) {
        returnRepository$$0 = returnRepository$$2;
    }

    @Override
    public void writeToParcel(android.os.Parcel parcel$$0, int flags) {
        write(returnRepository$$0, parcel$$0, flags, new IdentityCollection());
    }

    public static void write(com.marcela.challengeconcretesolutions.models.ReturnRepository returnRepository$$1, android.os.Parcel parcel$$1, int flags$$0, IdentityCollection identityMap$$0) {
        int identity$$0 = identityMap$$0 .getKey(returnRepository$$1);
        if (identity$$0 != -1) {
            parcel$$1 .writeInt(identity$$0);
        } else {
            parcel$$1 .writeInt(identityMap$$0 .put(returnRepository$$1));
            parcel$$1 .writeInt(InjectionUtil.getField(int.class, com.marcela.challengeconcretesolutions.models.ReturnRepository.class, returnRepository$$1, "total_count"));
            parcel$$1 .writeInt((InjectionUtil.getField(boolean.class, com.marcela.challengeconcretesolutions.models.ReturnRepository.class, returnRepository$$1, "incomplete_results")? 1 : 0));
            if (InjectionUtil.getField(java.util.ArrayList.class, com.marcela.challengeconcretesolutions.models.ReturnRepository.class, returnRepository$$1, "items") == null) {
                parcel$$1 .writeInt(-1);
            } else {
                parcel$$1 .writeInt(InjectionUtil.getField(java.util.ArrayList.class, com.marcela.challengeconcretesolutions.models.ReturnRepository.class, returnRepository$$1, "items").size());
                for (com.marcela.challengeconcretesolutions.models.Repository repository$$0 : ((java.util.ArrayList<com.marcela.challengeconcretesolutions.models.Repository> ) InjectionUtil.getField(java.util.ArrayList.class, com.marcela.challengeconcretesolutions.models.ReturnRepository.class, returnRepository$$1, "items"))) {
                    com.marcela.challengeconcretesolutions.models.Repository$$Parcelable.write(repository$$0, parcel$$1, flags$$0, identityMap$$0);
                }
            }
        }
    }

    @Override
    public int describeContents() {
        return  0;
    }

    @Override
    public com.marcela.challengeconcretesolutions.models.ReturnRepository getParcel() {
        return returnRepository$$0;
    }

    public static com.marcela.challengeconcretesolutions.models.ReturnRepository read(android.os.Parcel parcel$$3, IdentityCollection identityMap$$1) {
        int identity$$1 = parcel$$3 .readInt();
        if (identityMap$$1 .containsKey(identity$$1)) {
            if (identityMap$$1 .isReserved(identity$$1)) {
                throw new ParcelerRuntimeException("An instance loop was detected whild building Parcelable and deseralization cannot continue.  This error is most likely due to using @ParcelConstructor or @ParcelFactory.");
            }
            return identityMap$$1 .get(identity$$1);
        } else {
            com.marcela.challengeconcretesolutions.models.ReturnRepository returnRepository$$4;
            int reservation$$0 = identityMap$$1 .reserve();
            returnRepository$$4 = new com.marcela.challengeconcretesolutions.models.ReturnRepository();
            identityMap$$1 .put(reservation$$0, returnRepository$$4);
            InjectionUtil.setField(com.marcela.challengeconcretesolutions.models.ReturnRepository.class, returnRepository$$4, "total_count", parcel$$3 .readInt());
            InjectionUtil.setField(com.marcela.challengeconcretesolutions.models.ReturnRepository.class, returnRepository$$4, "incomplete_results", (parcel$$3 .readInt() == 1));
            int int$$0 = parcel$$3 .readInt();
            java.util.ArrayList<com.marcela.challengeconcretesolutions.models.Repository> list$$0;
            if (int$$0 < 0) {
                list$$0 = null;
            } else {
                list$$0 = new java.util.ArrayList<com.marcela.challengeconcretesolutions.models.Repository>(int$$0);
                for (int int$$1 = 0; (int$$1 <int$$0); int$$1 ++) {
                    com.marcela.challengeconcretesolutions.models.Repository repository$$1 = com.marcela.challengeconcretesolutions.models.Repository$$Parcelable.read(parcel$$3, identityMap$$1);
                    list$$0 .add(repository$$1);
                }
            }
            InjectionUtil.setField(com.marcela.challengeconcretesolutions.models.ReturnRepository.class, returnRepository$$4, "items", list$$0);
            com.marcela.challengeconcretesolutions.models.ReturnRepository returnRepository$$3 = returnRepository$$4;
            identityMap$$1 .put(identity$$1, returnRepository$$3);
            return returnRepository$$3;
        }
    }

}
