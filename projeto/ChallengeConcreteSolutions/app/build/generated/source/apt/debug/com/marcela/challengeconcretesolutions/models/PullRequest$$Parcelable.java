
package com.marcela.challengeconcretesolutions.models;

import android.os.Parcelable;
import android.os.Parcelable.Creator;
import org.parceler.Generated;
import org.parceler.IdentityCollection;
import org.parceler.InjectionUtil;
import org.parceler.ParcelWrapper;
import org.parceler.ParcelerRuntimeException;

@Generated(value = "org.parceler.ParcelAnnotationProcessor", date = "2016-12-19T08:07-0200")
@SuppressWarnings({
    "unchecked",
    "deprecation"
})
public class PullRequest$$Parcelable
    implements Parcelable, ParcelWrapper<com.marcela.challengeconcretesolutions.models.PullRequest>
{

    private com.marcela.challengeconcretesolutions.models.PullRequest pullRequest$$0;
    @SuppressWarnings("UnusedDeclaration")
    public final static Creator<PullRequest$$Parcelable>CREATOR = new Creator<PullRequest$$Parcelable>() {


        @Override
        public PullRequest$$Parcelable createFromParcel(android.os.Parcel parcel$$2) {
            return new PullRequest$$Parcelable(read(parcel$$2, new IdentityCollection()));
        }

        @Override
        public PullRequest$$Parcelable[] newArray(int size) {
            return new PullRequest$$Parcelable[size] ;
        }

    }
    ;

    public PullRequest$$Parcelable(com.marcela.challengeconcretesolutions.models.PullRequest pullRequest$$2) {
        pullRequest$$0 = pullRequest$$2;
    }

    @Override
    public void writeToParcel(android.os.Parcel parcel$$0, int flags) {
        write(pullRequest$$0, parcel$$0, flags, new IdentityCollection());
    }

    public static void write(com.marcela.challengeconcretesolutions.models.PullRequest pullRequest$$1, android.os.Parcel parcel$$1, int flags$$0, IdentityCollection identityMap$$0) {
        int identity$$0 = identityMap$$0 .getKey(pullRequest$$1);
        if (identity$$0 != -1) {
            parcel$$1 .writeInt(identity$$0);
        } else {
            parcel$$1 .writeInt(identityMap$$0 .put(pullRequest$$1));
            parcel$$1 .writeString(InjectionUtil.getField(java.lang.String.class, com.marcela.challengeconcretesolutions.models.PullRequest.class, pullRequest$$1, "html_url"));
            parcel$$1 .writeSerializable(InjectionUtil.getField(java.util.Date.class, com.marcela.challengeconcretesolutions.models.PullRequest.class, pullRequest$$1, "created_at"));
            parcel$$1 .writeInt(InjectionUtil.getField(int.class, com.marcela.challengeconcretesolutions.models.PullRequest.class, pullRequest$$1, "id"));
            parcel$$1 .writeString(InjectionUtil.getField(java.lang.String.class, com.marcela.challengeconcretesolutions.models.PullRequest.class, pullRequest$$1, "state"));
            parcel$$1 .writeString(InjectionUtil.getField(java.lang.String.class, com.marcela.challengeconcretesolutions.models.PullRequest.class, pullRequest$$1, "title"));
            parcel$$1 .writeString(InjectionUtil.getField(java.lang.String.class, com.marcela.challengeconcretesolutions.models.PullRequest.class, pullRequest$$1, "body"));
            com.marcela.challengeconcretesolutions.models.User$$Parcelable.write(InjectionUtil.getField(com.marcela.challengeconcretesolutions.models.User.class, com.marcela.challengeconcretesolutions.models.PullRequest.class, pullRequest$$1, "user"), parcel$$1, flags$$0, identityMap$$0);
        }
    }

    @Override
    public int describeContents() {
        return  0;
    }

    @Override
    public com.marcela.challengeconcretesolutions.models.PullRequest getParcel() {
        return pullRequest$$0;
    }

    public static com.marcela.challengeconcretesolutions.models.PullRequest read(android.os.Parcel parcel$$3, IdentityCollection identityMap$$1) {
        int identity$$1 = parcel$$3 .readInt();
        if (identityMap$$1 .containsKey(identity$$1)) {
            if (identityMap$$1 .isReserved(identity$$1)) {
                throw new ParcelerRuntimeException("An instance loop was detected whild building Parcelable and deseralization cannot continue.  This error is most likely due to using @ParcelConstructor or @ParcelFactory.");
            }
            return identityMap$$1 .get(identity$$1);
        } else {
            com.marcela.challengeconcretesolutions.models.PullRequest pullRequest$$4;
            int reservation$$0 = identityMap$$1 .reserve();
            pullRequest$$4 = new com.marcela.challengeconcretesolutions.models.PullRequest();
            identityMap$$1 .put(reservation$$0, pullRequest$$4);
            InjectionUtil.setField(com.marcela.challengeconcretesolutions.models.PullRequest.class, pullRequest$$4, "html_url", parcel$$3 .readString());
            InjectionUtil.setField(com.marcela.challengeconcretesolutions.models.PullRequest.class, pullRequest$$4, "created_at", ((java.util.Date) parcel$$3 .readSerializable()));
            InjectionUtil.setField(com.marcela.challengeconcretesolutions.models.PullRequest.class, pullRequest$$4, "id", parcel$$3 .readInt());
            InjectionUtil.setField(com.marcela.challengeconcretesolutions.models.PullRequest.class, pullRequest$$4, "state", parcel$$3 .readString());
            InjectionUtil.setField(com.marcela.challengeconcretesolutions.models.PullRequest.class, pullRequest$$4, "title", parcel$$3 .readString());
            InjectionUtil.setField(com.marcela.challengeconcretesolutions.models.PullRequest.class, pullRequest$$4, "body", parcel$$3 .readString());
            com.marcela.challengeconcretesolutions.models.User user$$0 = com.marcela.challengeconcretesolutions.models.User$$Parcelable.read(parcel$$3, identityMap$$1);
            InjectionUtil.setField(com.marcela.challengeconcretesolutions.models.PullRequest.class, pullRequest$$4, "user", user$$0);
            com.marcela.challengeconcretesolutions.models.PullRequest pullRequest$$3 = pullRequest$$4;
            identityMap$$1 .put(identity$$1, pullRequest$$3);
            return pullRequest$$3;
        }
    }

}
