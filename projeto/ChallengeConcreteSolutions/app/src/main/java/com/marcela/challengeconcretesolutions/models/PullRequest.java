package com.marcela.challengeconcretesolutions.models;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.Date;

/**
 * Created by marcela on 15/12/16.
 */

@Parcel
public class PullRequest {
    @SerializedName("id")
    private int id;
    @SerializedName("html_url")
    private String html_url;
    @SerializedName("title")
    private String title;
    @SerializedName("body")
    private String body;
    @SerializedName("state")
    private String state;
    @SerializedName("user")
    private User user;
    @SerializedName("created_at")
    private Date created_at;

    public PullRequest() {
        super();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHtml_url() {
        return html_url;
    }

    public void setHtml_url(String html_url) {
        this.html_url = html_url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }
}
