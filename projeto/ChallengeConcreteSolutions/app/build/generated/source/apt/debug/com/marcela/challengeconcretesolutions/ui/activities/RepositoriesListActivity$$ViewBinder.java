// Generated code from Butter Knife. Do not modify!
package com.marcela.challengeconcretesolutions.ui.activities;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class RepositoriesListActivity$$ViewBinder<T extends com.marcela.challengeconcretesolutions.ui.activities.RepositoriesListActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131492978, "field 'toolbar'");
    target.toolbar = finder.castView(view, 2131492978, "field 'toolbar'");
    view = finder.findRequiredView(source, 2131492983, "field 'progress'");
    target.progress = finder.castView(view, 2131492983, "field 'progress'");
    view = finder.findRequiredView(source, 2131492985, "field 'rv_repositories'");
    target.rv_repositories = finder.castView(view, 2131492985, "field 'rv_repositories'");
  }

  @Override public void unbind(T target) {
    target.toolbar = null;
    target.progress = null;
    target.rv_repositories = null;
  }
}
