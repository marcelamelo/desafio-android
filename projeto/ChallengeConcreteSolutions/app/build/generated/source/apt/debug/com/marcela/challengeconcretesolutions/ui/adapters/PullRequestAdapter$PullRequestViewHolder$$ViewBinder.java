// Generated code from Butter Knife. Do not modify!
package com.marcela.challengeconcretesolutions.ui.adapters;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class PullRequestAdapter$PullRequestViewHolder$$ViewBinder<T extends com.marcela.challengeconcretesolutions.ui.adapters.PullRequestAdapter.PullRequestViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131493025, "field 'pull_request_row'");
    target.pull_request_row = finder.castView(view, 2131493025, "field 'pull_request_row'");
    view = finder.findRequiredView(source, 2131493027, "field 'txt_pull_name'");
    target.txt_pull_name = finder.castView(view, 2131493027, "field 'txt_pull_name'");
    view = finder.findRequiredView(source, 2131493029, "field 'txt_pull_description'");
    target.txt_pull_description = finder.castView(view, 2131493029, "field 'txt_pull_description'");
    view = finder.findRequiredView(source, 2131493028, "field 'txt_pull_date'");
    target.txt_pull_date = finder.castView(view, 2131493028, "field 'txt_pull_date'");
    view = finder.findRequiredView(source, 2131493031, "field 'img_user_avatar'");
    target.img_user_avatar = finder.castView(view, 2131493031, "field 'img_user_avatar'");
    view = finder.findRequiredView(source, 2131493032, "field 'txt_user_username'");
    target.txt_user_username = finder.castView(view, 2131493032, "field 'txt_user_username'");
    view = finder.findRequiredView(source, 2131493033, "field 'txt_user_fullname'");
    target.txt_user_fullname = finder.castView(view, 2131493033, "field 'txt_user_fullname'");
  }

  @Override public void unbind(T target) {
    target.pull_request_row = null;
    target.txt_pull_name = null;
    target.txt_pull_description = null;
    target.txt_pull_date = null;
    target.img_user_avatar = null;
    target.txt_user_username = null;
    target.txt_user_fullname = null;
  }
}
