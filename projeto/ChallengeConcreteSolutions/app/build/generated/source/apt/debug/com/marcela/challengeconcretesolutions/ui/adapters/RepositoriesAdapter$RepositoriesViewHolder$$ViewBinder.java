// Generated code from Butter Knife. Do not modify!
package com.marcela.challengeconcretesolutions.ui.adapters;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class RepositoriesAdapter$RepositoriesViewHolder$$ViewBinder<T extends com.marcela.challengeconcretesolutions.ui.adapters.RepositoriesAdapter.RepositoriesViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131493034, "field 'repository_row'");
    target.repository_row = finder.castView(view, 2131493034, "field 'repository_row'");
    view = finder.findRequiredView(source, 2131493037, "field 'txt_repository_name'");
    target.txt_repository_name = finder.castView(view, 2131493037, "field 'txt_repository_name'");
    view = finder.findRequiredView(source, 2131493038, "field 'txt_repository_description'");
    target.txt_repository_description = finder.castView(view, 2131493038, "field 'txt_repository_description'");
    view = finder.findRequiredView(source, 2131493039, "field 'txt_count_fork'");
    target.txt_count_fork = finder.castView(view, 2131493039, "field 'txt_count_fork'");
    view = finder.findRequiredView(source, 2131493040, "field 'txt_count_star'");
    target.txt_count_star = finder.castView(view, 2131493040, "field 'txt_count_star'");
    view = finder.findRequiredView(source, 2131493041, "field 'img_owner_avatar'");
    target.img_owner_avatar = finder.castView(view, 2131493041, "field 'img_owner_avatar'");
    view = finder.findRequiredView(source, 2131493042, "field 'txt_owner_username'");
    target.txt_owner_username = finder.castView(view, 2131493042, "field 'txt_owner_username'");
    view = finder.findRequiredView(source, 2131493043, "field 'txt_owner_fullname'");
    target.txt_owner_fullname = finder.castView(view, 2131493043, "field 'txt_owner_fullname'");
  }

  @Override public void unbind(T target) {
    target.repository_row = null;
    target.txt_repository_name = null;
    target.txt_repository_description = null;
    target.txt_count_fork = null;
    target.txt_count_star = null;
    target.img_owner_avatar = null;
    target.txt_owner_username = null;
    target.txt_owner_fullname = null;
  }
}
