package com.marcela.challengeconcretesolutions.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.marcela.challengeconcretesolutions.R;
import com.marcela.challengeconcretesolutions.commons.CircleTransform;
import com.marcela.challengeconcretesolutions.commons.RecyclerViewListener;
import com.marcela.challengeconcretesolutions.models.PullRequest;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by marcela on 16/12/16.
 */

public class PullRequestAdapter extends RecyclerView.Adapter<PullRequestAdapter.PullRequestViewHolder>{

    public class PullRequestViewHolder extends RecyclerView.ViewHolder{

        private Context context;

        @Bind(R.id.pull_request_row)
        RelativeLayout pull_request_row;
        @Bind(R.id.txt_pull_name)
        TextView txt_pull_name;
        @Bind(R.id.txt_pull_description)
        TextView txt_pull_description;
        @Bind(R.id.txt_pull_date)
        TextView txt_pull_date;
        @Bind(R.id.img_user_avatar)
        ImageView img_user_avatar;
        @Bind(R.id.txt_user_username)
        TextView txt_user_username;
        @Bind(R.id.txt_user_fullname)
        TextView txt_user_fullname;


        public PullRequestViewHolder(Context context, View itemView) {
            super(itemView);
            this.context = context;
            ButterKnife.bind(this, itemView);
        }
    }

    private Context context;
    private ArrayList<PullRequest> items;
    RecyclerViewListener listener;

    public PullRequestAdapter(Context context, ArrayList<PullRequest> items, RecyclerViewListener listener) {
        this.context = context;
        this.items = items;
        this.listener = listener;
    }

    @Override
    public PullRequestViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.pull_request_item, parent, false);
        PullRequestViewHolder viewHolder = new PullRequestViewHolder(context, view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(PullRequestViewHolder holder, final int position) {
        SimpleDateFormat sfd = new SimpleDateFormat("dd/MM/yyyy");

        PullRequest pullRequest = items.get(position);
        Glide.with(context).load(pullRequest.getUser().getAvatar_url()).centerCrop().transform(new CircleTransform(context)).skipMemoryCache(false).diskCacheStrategy(DiskCacheStrategy.RESULT).into(holder.img_user_avatar);
        holder.txt_pull_name.setText(pullRequest.getTitle());
        holder.txt_pull_description.setText(pullRequest.getBody());
        holder.txt_pull_date.setText(sfd.format(pullRequest.getCreated_at()));
        holder.txt_user_username.setText(pullRequest.getUser().getLogin());
        holder.txt_user_fullname.setText(pullRequest.getUser().getName());

        holder.pull_request_row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClick(position);
            }
        });
    }


    @Override
    public int getItemCount() {
        return items.size();
    }
}
