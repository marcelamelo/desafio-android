package com.marcela.challengeconcretesolutions.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RelativeLayout;

import com.marcela.challengeconcretesolutions.R;
import com.marcela.challengeconcretesolutions.commons.EndlessRecyclerViewScrollListener;
import com.marcela.challengeconcretesolutions.commons.RecyclerViewListener;
import com.marcela.challengeconcretesolutions.commons.ResponseListener;
import com.marcela.challengeconcretesolutions.controller.RepositoryController;
import com.marcela.challengeconcretesolutions.models.Repository;
import com.marcela.challengeconcretesolutions.models.ReturnRepository;
import com.marcela.challengeconcretesolutions.ui.adapters.RepositoriesAdapter;

import org.parceler.Parcels;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

public class RepositoriesListActivity extends BaseActivity {

    private EndlessRecyclerViewScrollListener scrollListener;

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.progress)
    RelativeLayout progress;
    @Bind(R.id.rv_repositories)
    RecyclerView rv_repositories;

    ReturnRepository returnRepository;
    ArrayList<Repository> allRepositories = new ArrayList<>();
    ArrayList<Repository> moreRepositories = new ArrayList<>();

    RepositoriesAdapter adapter;

    Repository repository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repositories_list);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setIcon(this.getResources().getDrawable(R.drawable.ic_menu));
        getSupportActionBar().setTitle("Github JavaPop");

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rv_repositories.setLayoutManager(linearLayoutManager);

        progress.setVisibility(View.VISIBLE);
        getData();

        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                progress.setVisibility(View.VISIBLE);
                loadMoreData(page, view);
            }
        };

        rv_repositories.addOnScrollListener(scrollListener);
        rv_repositories.setItemAnimator(new DefaultItemAnimator());

    }

    public void getData(){
        new RepositoryController().getRepositories(0, new ResponseListener() {
            @Override
            public void success(Object object) {
                returnRepository = (ReturnRepository)object;
                allRepositories = returnRepository.getItems();
                progress.setVisibility(View.GONE);
                adapter = new RepositoriesAdapter(RepositoriesListActivity.this, allRepositories, new RecyclerViewListener() {
                    @Override
                    public void onClick(int position) {
                        repository = allRepositories.get(position);
                        Intent intent = new Intent(RepositoriesListActivity.this, PullRequestsListActivity.class);
                        intent.putExtra("repository", Parcels.wrap(Repository.class, repository));
                        startActivity(intent);
                    }
                });
                rv_repositories.setAdapter(adapter);
            }

            @Override
            public void fail(Throwable throwable) {
                progress.setVisibility(View.GONE);
                if (isOline(RepositoriesListActivity.this)) {
                    showDialogMessage("", throwable.getMessage());
                } else {
                    showDialogMessage("Falha de conexão", "Verifique sua internet e tente novamente.");
                    finish();
                }
            }
        });
    }

    public void loadMoreData(final int page, final RecyclerView view){
        new RepositoryController().getRepositories(page + 1, new ResponseListener() {
            @Override
            public void success(Object object) {
                returnRepository = (ReturnRepository)object;
                moreRepositories = returnRepository.getItems();
                final int curSize = adapter.getItemCount();
                allRepositories.addAll(moreRepositories);

                view.post(new Runnable() {
                    @Override
                    public void run() {
                        progress.setVisibility(View.GONE);
                        adapter.notifyItemRangeInserted(curSize, allRepositories.size() -1);
                    }
                });
            }

            @Override
            public void fail(Throwable throwable) {
                progress.setVisibility(View.GONE);
                if (isOline(RepositoriesListActivity.this)) {
                    showDialogMessage("", throwable.getMessage());
                } else {
                    showDialogMessage("Falha de conexão", "Verifique sua internet e tente novamente.");
                }
            }
        });
    }
}

